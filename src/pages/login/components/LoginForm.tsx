import React, { useCallback } from "react";
import { Form, Input, Typography, Button } from "antd";
import { useForm } from "antd/lib/form/Form";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { useRouter } from "next/navigation";

import { brandColor } from "@/utils/constants";
import { useLogin } from "@/hooks/auth";

const LoginForm = () => {
  const [form] = useForm();
  const router = useRouter();

  const { login, isLoggingin } = useLogin({});

  const handleSubmit = useCallback(async () => {
    if (isLoggingin) return;
    const [values] = await Promise.all([form.validateFields()]);
    const { userName, password } = values;
    login({
      userName,
      password,
    });
  }, [form, isLoggingin, login]);

  const handleNavigateToSignUpPage = useCallback(() => {
    router.push("/register");
  }, [router]);

  return (
    <div className="flex justify-center items-center w-4/12 p-16">
      <div className="flex flex-col items-center w-full">
        <Typography.Text
          className="!text-2xl font-bold mb-16"
          style={{ color: brandColor }}
        >
          Login
        </Typography.Text>
        <Form form={form} layout="vertical" className="w-full">
          <Form.Item
            label="Username"
            name="userName"
            rules={[{ required: true, message: "Username is required!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              className="!p-2"
            />
          </Form.Item>
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Password is required!" }]}
            className="m-0"
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              className="!p-2"
            />
          </Form.Item>
        </Form>

        <Button
          className="w-full !rounded-lg mt-6 !text-white"
          style={{ background: brandColor, height: "2.75rem" }}
          loading={isLoggingin}
          onClick={handleSubmit}
        >
          <Typography.Text className="font-bold uppercase !text-white">
            Login
          </Typography.Text>
        </Button>
      </div>
    </div>
  );
};

export default LoginForm;
