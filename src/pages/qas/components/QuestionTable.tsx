import { Dispatch, SetStateAction } from "react";
import { Popconfirm, Space, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { ColumnsType, TablePaginationConfig } from "antd/lib/table";
import moment from "moment";

import QA from "@/models/QA";
import { useDeleteQA } from "@/hooks/qas";

interface QATableProps {
  data: QA[];
  isLoading: boolean;
  pagination: TablePaginationConfig;
  onSetPagination: Dispatch<SetStateAction<TablePaginationConfig>>;
  onEditButtonClick: (qaId: string) => void;
}

const QATable = ({
  data,
  isLoading,
  pagination,
  onSetPagination,
  onEditButtonClick,
}: QATableProps) => {
  const { deleteQA } = useDeleteQA({});

  const columns: ColumnsType<QA> = [
    {
      title: "Question",
      dataIndex: "questionContent",
      key: "questionContent",
    },
    {
      title: "Answer",
      dataIndex: "answerContent",
      key: "answerContent",
    },
    {
      title: "Topic",
      dataIndex: "topic",
      key: "topic",
    },
    {
      title: "Created At",
      key: "createdAt",
      dataIndex: "createdAt",
      render: (createdAt) => moment(createdAt).format("YYYY-MM-DD HH:mm"),
    },
    {
      title: "Last Updated At",
      key: "updatedAt",
      dataIndex: "updatedAt",
      render: (updatedAt) => moment(updatedAt).format("YYYY-MM-DD HH:mm"),
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "id",
      align: "center",
      render: (id) => (
        <Space>
          <EditOutlined
            className="hover:text-primary mr-2"
            onClick={() => onEditButtonClick(id)}
          />
          <Popconfirm
            title="Are you sure you want to delete this Q&A?"
            onConfirm={() => deleteQA({ qaId: id })}
            okText="Yes"
            cancelText="No"
            okType="danger"
          >
            <DeleteOutlined className="hover:text-primary" />
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <Table<QA>
      columns={columns}
      dataSource={data}
      pagination={pagination}
      onChange={onSetPagination}
      loading={isLoading}
      rowKey={(qa) => qa.id}
      bordered
    />
  );
};

export default QATable;
