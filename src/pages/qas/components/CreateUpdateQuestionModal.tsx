import { useEffect, useCallback, useMemo } from "react";
import { Modal, Form, Input, Spin } from "antd";
import { useForm } from "antd/lib/form/Form";

import { useGetQADetail, useUpdateQA, useCreateQA } from "@/hooks/qas";

interface CreateUpdateQAModalProps {
  qaId: string;
  isOpen: boolean;
  onClose: () => void;
}

const CreateUpdateQAModal = ({
  qaId,
  isOpen,
  onClose,
}: CreateUpdateQAModalProps) => {
  const [form] = useForm();

  const { qa, isLoadingQADetail } = useGetQADetail({
    qaId,
    enabled: isOpen && !!qaId,
  });

  const handleClose = useCallback(() => {
    form.resetFields();
    onClose();
  }, [form, onClose]);

  const isCreate = useMemo(() => !qaId, [qaId]);

  const { createQA, isCreatingQA } = useCreateQA({
    onSuccess: handleClose,
  });
  const { updateQA, isUpdatingQA } = useUpdateQA({
    onSuccess: handleClose,
  });

  useEffect(() => {
    if (qaId && qa) {
      form.setFieldsValue({
        questionContent: qa.questionContent,
        answerContent: qa.answerContent,
        topic: qa.topic,
      });
    }
  }, [form, qaId, qa]);

  const handleSubmit = useCallback(async () => {
    const [values] = await Promise.all([form.validateFields()]);
    const { questionContent, answerContent, topic } = values;
    if (isCreate) {
      createQA({
        questionContent,
        answerContent,
        topic,
      });
    } else {
      updateQA({
        ...qa,
        questionContent,
        answerContent,
        topic,
      });
    }
  }, [isCreate, form, qa, createQA, updateQA]);

  return (
    <Modal
      open={isOpen}
      title={isCreate ? "Create New Q&A" : "Update Q&A"}
      okText={isCreate ? "Create" : "Update"}
      okType="default"
      okButtonProps={{
        className: "bg-blue-500 !text-white",
        disabled: isCreatingQA || isUpdatingQA,
      }}
      onOk={handleSubmit}
      onCancel={handleClose}
    >
      <Spin spinning={!isCreate && isLoadingQADetail}>
        <Form form={form} layout="vertical" className="mt-8">
          <Form.Item
            label="Question"
            name="questionContent"
            rules={[{ required: true, message: "Question is required!" }]}
          >
            <Input.TextArea className="p-2" />
          </Form.Item>
          <Form.Item
            label="Answer"
            name="answerContent"
            rules={[{ required: true, message: "Answer is required!" }]}
          >
            <Input.TextArea className="p-2" />
          </Form.Item>
          <Form.Item
            label="Topic"
            name="topic"
            rules={[{ required: true, message: "Topic is required!" }]}
          >
            <Input className="p-2" />
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};

export default CreateUpdateQAModal;
