import { useState, useMemo } from "react";
import { Typography, Button, TablePaginationConfig } from "antd";

import QuestionTable from "./components/QuestionTable";
import CreateUpdateQuestionModal from "./components/CreateUpdateQuestionModal";
import { SearchBar } from "@/components";
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE,
  DEFAULT_PAGINATION,
} from "@/utils/constants";
import { useGetAllQAs } from "@/hooks/qas";

const QuestionManagement = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [selectedQAId, setSelectedQAId] = useState<string>("");
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [pagination, setPagination] =
    useState<TablePaginationConfig>(DEFAULT_PAGINATION);

  const {
    qas = [],
    total,
    isLoadingQAs,
  } = useGetAllQAs({
    searchTerm,
    pageNumber: pagination.current || DEFAULT_PAGE_NUMBER,
    pageSize: pagination.pageSize || DEFAULT_PAGE_SIZE,
  });

  const tablePagination = useMemo(() => {
    return { ...pagination, total };
  }, [pagination, total]);

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleEditButtonClick = (questionId: string) => {
    setSelectedQAId(questionId);
    handleOpenModal();
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
    setSelectedQAId("");
  };

  return (
    <>
      <Typography.Title level={3}>Q&A Management</Typography.Title>
      <div className="flex">
        <div className="w-64">
          <SearchBar
            placeholder="Search by question, answer"
            onSetSearchTerm={setSearchTerm}
          />
        </div>
        <Button
          className="bg-blue-500 !text-white ml-2"
          onClick={handleOpenModal}
        >
          Create New
        </Button>
      </div>
      <div className="mt-4">
        <QuestionTable
          data={qas}
          isLoading={isLoadingQAs}
          pagination={tablePagination}
          onSetPagination={setPagination}
          onEditButtonClick={handleEditButtonClick}
        />
      </div>
      <CreateUpdateQuestionModal
        qaId={selectedQAId}
        isOpen={isModalOpen}
        onClose={handleCloseModal}
      />
    </>
  );
};

export default QuestionManagement;
