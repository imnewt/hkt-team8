import { CSSProperties } from "react";
import { Col, Flex, Row, Spin, Typography, Image } from "antd";
import moment from "moment";

const loadingOverlayStyle: CSSProperties = {
  width: "20rem",
  height: "18rem",
  position: "relative",
};

const overlayBeforeStyle: CSSProperties = {
  content: '""',
  position: "absolute",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  background: "rgba(0, 0, 0, 0.2)", // Adjust the opacity or color as needed
};

const foodChoiceHabitAndAllergyAnalysis = [
  {
    url: "https://drive.google.com/uc?export=view&id=1-AD74H2bh6b__NO0R6axtREM6r332nVX",
    alt: "Distribution of countries",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1-Fmz5dj_RVAAdNWSrZ_tee8Xbiv3tCrA",
    alt: "Distribution of allergies",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1-GZyvW7IYCffLUIdw-6wQ0rbmmHB_0-T",
    alt: "Distribution of Food Names",
  },
];

const eventExperienceInsights = [
  {
    url: "https://drive.google.com/uc?export=view&id=1BqDZMom-aq2bMDL7aq0JiWA3YszbFDeg",
    alt: "Count Chart for Communication Preference",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1JyiLsWrAIqVrmySlefkcghTszsQ8p6L1",
    alt: "Count Chart for CS Friendliness and Professionalism",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1-YOzW3Vr5cQuoUhNfwwoy7eACIBoNfZ2",
    alt: "Count Chart for Effectiveness of Organizer",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1bl3kZhg-Db-Og4nyKBpIvuDgXCfgOz5d",
    alt: "Count Chart for Overall Satisfaction",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1OqDe0KzHmhhcNzvU5spK_4GXJeP_Inw3",
    alt: "Count Chart for Satisfaction with Breaks",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=14FBwCfO5UonsV_PIqh_gox4Vyn0Ip_VJ",
    alt: "Count Chart for Satisfaction with Follow-up",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1n50rFFm1Gfg8oS-SUNTQl-is7IxSHBeE",
    alt: "Appropriate Duration",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1mzverd3IvNaueG0glnlYPV3CMfuD1tha",
    alt: "Clarity of Materials",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1-3rRMXvR6CxaWe1OrTRfLj1EnoNY1sMg",
    alt: "Communication Adequacy",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=14WDp-F6h7mOxnNrOxCuqAk3xfQG-fpau",
    alt: "Event Organization",
  },
  {
    url: "https://drive.google.com/uc?export=view&id=1-ALwDouDDaRlhocQK7XA5uKnEzU9dFYW",
    alt: "Interest of Future Events",
  },
];

const predictions = [
  {
    url: "https://drive.google.com/uc?export=view&id=1-ChN_3CkIqI6kduex5AYsX5zk4lPAoxI",
    alt: "Distribution of Return rates",
  },
];

const Insights = () => {
  const currentDate = moment().format("YYYYMMDD");

  return (
    <>
      <Typography.Title level={3}>Insights</Typography.Title>
      <p className="mt-8 mb-4 italic font-semibold">
        Food Choice Habit & Allergy Analysis
      </p>
      <Row gutter={[16, 16]} className="">
        {foodChoiceHabitAndAllergyAnalysis.map((chart) => (
          <Col span={8} key={chart.url}>
            <div className="border rounded-lg p-4 text-center shadow-lg">
              <Image
                src={`${chart.url}&refresh=${currentDate}`}
                alt={chart.alt}
                width="20rem"
                height="18rem"
                placeholder={
                  <Flex
                    justify="center"
                    align="center"
                    style={loadingOverlayStyle}
                  >
                    <Spin size="large" />
                    <div style={overlayBeforeStyle}></div>
                  </Flex>
                }
              />
            </div>
          </Col>
        ))}
      </Row>
      <p className="mt-8 mb-4 italic font-semibold">
        Event Experience Insights
      </p>
      <Row gutter={[16, 16]} className="">
        {eventExperienceInsights.map((chart) => (
          <Col span={8} key={chart.url}>
            <div className="border rounded-lg p-4 text-center shadow-lg">
              <Image
                src={`${chart.url}&refresh=${currentDate}`}
                alt={chart.alt}
                width="20rem"
                height="18rem"
                placeholder={
                  <Flex
                    justify="center"
                    align="center"
                    style={loadingOverlayStyle}
                  >
                    <Spin size="large" />
                    <div style={overlayBeforeStyle}></div>
                  </Flex>
                }
              />
            </div>
          </Col>
        ))}
      </Row>
      <p className="mt-8 mb-4 italic font-semibold">Prediction of Return</p>
      <Row gutter={[16, 16]} className="">
        {predictions.map((chart) => (
          <Col span={8} key={chart.url}>
            <div className="border rounded-lg p-4 text-center shadow-lg">
              <Image
                src={`${chart.url}&refresh=${currentDate}`}
                alt={chart.alt}
                width="20rem"
                height="18rem"
                placeholder={
                  <Flex
                    justify="center"
                    align="center"
                    style={loadingOverlayStyle}
                  >
                    <Spin size="large" />
                    <div style={overlayBeforeStyle}></div>
                  </Flex>
                }
              />
            </div>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default Insights;
