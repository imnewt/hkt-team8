import { useEffect, useCallback, useMemo } from "react";
import { Typography, Modal, Form, Input, Spin, Select, DatePicker } from "antd";
import { useForm } from "antd/lib/form/Form";
import { UserOutlined } from "@ant-design/icons";
import moment from "moment";

import { useGetUserDetail, useUpdateUser, useCreateUser } from "@/hooks/users";
import { useGetAllRoles } from "@/hooks/roles";
import { USER_ROLE_ID } from "@/utils/constants";

interface CreateUpdateUserModalProps {
  userId: string;
  isOpen: boolean;
  onClose: () => void;
}

const CreateUpdateUserModal = ({
  userId,
  isOpen,
  onClose,
}: CreateUpdateUserModalProps) => {
  const [form] = useForm();

  const { user, isLoadingUserDetail } = useGetUserDetail({
    userId,
    enabled: isOpen && !!userId,
  });
  const { roles = [], isLoadingRoles } = useGetAllRoles();

  const handleClose = useCallback(() => {
    form.resetFields();
    onClose();
  }, [form, onClose]);

  const isCreate = useMemo(() => !userId, [userId]);

  const roleOptions = useMemo(() => {
    return roles?.map((role) => ({ value: role.name, label: role.name })) || [];
  }, [roles]);

  const { createUser, isCreatingUser } = useCreateUser({
    onSuccess: handleClose,
  });
  const { updateUser, isUpdatingUser } = useUpdateUser({
    onSuccess: handleClose,
  });

  useEffect(() => {
    if (isCreate) {
      form.setFieldsValue({
        roleNames: ["Guest"],
      });
    }
  }, [form, isCreate]);

  useEffect(() => {
    if (userId && user) {
      form.setFieldsValue({
        userName: user.userName,
        roleNames: user.roles.map((role) => role.name),
        timeExistAcc: moment(user.timeExistAcc),
      });
    }
  }, [form, userId, user]);

  const handleSubmit = useCallback(async () => {
    const [values] = await Promise.all([form.validateFields()]);
    const { userName, roleNames, timeExistAcc } = values;
    const roleIds = roles.reduce<string[]>(
      (ids, role) => (roleNames.includes(role.name) ? [...ids, role.id] : ids),
      []
    );
    if (isCreate) {
      createUser({
        userName,
        password: userName,
        timeExistAcc,
        roleIds: [USER_ROLE_ID],
      });
    } else {
      updateUser({
        ...user,
        userName,
        timeExistAcc,
        roleIds,
      });
    }
  }, [isCreate, form, user, roles, createUser, updateUser]);

  return (
    <Modal
      open={isOpen}
      title={isCreate ? "Create New User" : "Update User"}
      okText={isCreate ? "Create" : "Update"}
      okType="default"
      okButtonProps={{
        className: "bg-blue-500 !text-white",
        disabled: isCreatingUser || isUpdatingUser,
      }}
      onOk={handleSubmit}
      onCancel={handleClose}
    >
      <Spin spinning={!isCreate && isLoadingUserDetail}>
        <Form form={form} layout="vertical" className="mt-8 w-72">
          <Form.Item
            label="Username"
            name="userName"
            rules={[{ required: true, message: "Username is required!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              className="p-2"
            />
          </Form.Item>
          <Form.Item
            label="Roles"
            name="roleNames"
            rules={[
              { required: true, message: "At least 1 role is required!" },
            ]}
          >
            <Select
              className="antd-select"
              allowClear
              style={{ width: "100%" }}
              size="large"
              placeholder={
                <span className="text-sm">Please select 1 role</span>
              }
              disabled
              options={roleOptions}
              loading={isLoadingRoles}
            />
          </Form.Item>
          <Form.Item
            label="Existence time"
            name="timeExistAcc"
            rules={[{ required: true, message: "Existence time is required!" }]}
            className="m-0"
          >
            <DatePicker size="large" />
          </Form.Item>
        </Form>
        {isCreate && (
          <div className="flex flex-col mt-4">
            <Typography.Text className="text-yellow-400">
              <span className="mr-1">*</span>Default password will be your typed
              username
            </Typography.Text>
          </div>
        )}
      </Spin>
    </Modal>
  );
};

export default CreateUpdateUserModal;
