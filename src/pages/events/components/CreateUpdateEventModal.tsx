import { useEffect, useCallback, useMemo } from "react";
import { Modal, Form, Input, Spin, DatePicker } from "antd";
import { useForm } from "antd/lib/form/Form";
import moment from "moment";

import {
  useGetEventDetail,
  useUpdateEvent,
  useCreateEvent,
} from "@/hooks/events";

interface CreateUpdateEventModalProps {
  eventId: string;
  isOpen: boolean;
  onClose: () => void;
}

const CreateUpdateEventModal = ({
  eventId,
  isOpen,
  onClose,
}: CreateUpdateEventModalProps) => {
  const [form] = useForm();

  const { event, isLoadingEventDetail } = useGetEventDetail({
    eventId,
    enabled: isOpen && !!eventId,
  });

  const handleClose = useCallback(() => {
    form.resetFields();
    onClose();
  }, [form, onClose]);

  const isCreate = useMemo(() => !eventId, [eventId]);

  const { createEvent, isCreatingEvent } = useCreateEvent({
    onSuccess: handleClose,
  });
  const { updateEvent, isUpdatingEvent } = useUpdateEvent({
    onSuccess: handleClose,
  });

  useEffect(() => {
    if (eventId && event) {
      form.setFieldsValue({
        name: event.name,
        description: event.description,
        startTime: moment(event.startTime),
      });
    }
  }, [form, eventId, event]);

  const handleSubmit = useCallback(async () => {
    const [values] = await Promise.all([form.validateFields()]);
    const { name, description, startTime } = values;
    if (isCreate) {
      createEvent({
        name,
        description,
        startTime,
      });
    } else {
      updateEvent({
        ...event,
        name,
        description,
        startTime,
      });
    }
  }, [isCreate, form, event, createEvent, updateEvent]);

  return (
    <Modal
      open={isOpen}
      title={isCreate ? "Create New Event" : "Update Event"}
      okText={isCreate ? "Create" : "Update"}
      okType="default"
      okButtonProps={{
        className: "bg-blue-500 !text-white",
        disabled: isCreatingEvent || isUpdatingEvent,
      }}
      onOk={handleSubmit}
      onCancel={handleClose}
      width={600}
    >
      <Spin spinning={!isCreate && isLoadingEventDetail}>
        <Form form={form} layout="vertical" className="mt-8">
          <Form.Item
            label="Event name"
            name="name"
            rules={[{ required: true, message: "Event name is required!" }]}
          >
            <Input className="p-2" />
          </Form.Item>
          <Form.Item
            label="Description"
            name="description"
            rules={[{ required: true, message: "Description is required!" }]}
          >
            <Input.TextArea className="p-2" />
          </Form.Item>
          <Form.Item
            label="Start time"
            name="startTime"
            rules={[{ required: true, message: "Start time is required!" }]}
          >
            <DatePicker showTime className="p-2" />
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};

export default CreateUpdateEventModal;
