import { Dispatch, SetStateAction } from "react";
import { Popconfirm, Space, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { ColumnsType, TablePaginationConfig } from "antd/lib/table";
import moment from "moment";

import Event from "@/models/Event";
import { useDeleteEvent } from "@/hooks/events";

interface EventTableProps {
  data: Event[];
  isLoading: boolean;
  pagination: TablePaginationConfig;
  onSetPagination: Dispatch<SetStateAction<TablePaginationConfig>>;
  onEditButtonClick: (eventId: string) => void;
}

const EventTable = ({
  data,
  isLoading,
  pagination,
  onSetPagination,
  onEditButtonClick,
}: EventTableProps) => {
  const { deleteEvent } = useDeleteEvent({});

  const columns: ColumnsType<Event> = [
    {
      title: "Event name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Start time",
      dataIndex: "startTime",
      key: "startTime",
      render: (startTime) => moment(startTime).format("YYYY-MM-DD HH:mm"),
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "id",
      align: "center",
      render: (id) => (
        <Space>
          <EditOutlined
            className="hover:text-primary mr-2"
            onClick={() => onEditButtonClick(id)}
          />
          <Popconfirm
            title="Are you sure you want to delete this event?"
            onConfirm={() => deleteEvent({ eventId: id })}
            okText="Yes"
            cancelText="No"
            okType="danger"
          >
            <DeleteOutlined className="hover:text-primary" />
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <Table<Event>
      columns={columns}
      dataSource={data}
      pagination={pagination}
      onChange={onSetPagination}
      loading={isLoading}
      rowKey={(event) => event.id}
      bordered
    />
  );
};

export default EventTable;
