export default interface RegisterRequest {
  userName: string;
  password: string;
}
