import Base from "./Base";
import Role from "./Role";

export interface NewUser {
  userName: string;
  password: string;
  timeExistAcc: Date;
  roleIds: string[];
}

export default interface User extends NewUser, Base {
  id: string;
  isActive: boolean;
  roles: Role[];
}
