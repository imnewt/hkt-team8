import Base from "./Base";

export interface NewQA {
  questionContent: string;
  answerContent: string;
  topic: string;
}

export default interface QA extends NewQA, Base {
  id: string;
}
