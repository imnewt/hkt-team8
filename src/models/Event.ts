import Base from "./Base";

export interface NewEvent {
  name: string;
  description: string;
  startTime: Date;
}

export default interface Event extends NewEvent, Base {
  id: string;
}
