import React, { useEffect, useState, useMemo } from "react";
import { Layout, Menu, theme } from "antd";
import {
  TeamOutlined,
  LogoutOutlined,
  QuestionCircleOutlined,
  CalendarOutlined,
  FormOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import { isEmpty } from "lodash";

import Logo from "public/images/logo.png";
import TransparentLogo from "public/images/logo-transparent.png";
import { brandColor } from "@/utils/constants";
import { getCurrentUser, logout, setCurrentUser } from "@/utils/functions";
import { useGetUserPermissions } from "@/hooks/permissions";

const { Header, Sider, Content } = Layout;

interface LayoutProps {
  children: React.ReactNode;
}

const MainLayout = ({ children }: LayoutProps) => {
  const router = useRouter();
  const currentUser = getCurrentUser();
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const [mounted, setMounted] = useState<boolean>(false);

  const { userPermissions = [] } = useGetUserPermissions(currentUser.id);

  useEffect(() => {
    if (currentUser.id && !isEmpty(userPermissions)) {
      setCurrentUser({
        id: currentUser.id,
        permissions: userPermissions,
      });
    }
  }, [currentUser.id, userPermissions]);

  useEffect(() => {
    setMounted(true);
  }, []);

  const selectedMenu = useMemo(() => {
    return router.pathname.split("/")[1];
  }, [router]);

  if (!mounted) return <></>;
  return (
    <Layout className="min-h-screen" hasSider>
      <Sider
        className="shadow-md"
        style={{
          background: brandColor,
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
          top: 0,
          bottom: 0,
        }}
      >
        <div className="flex justify-center border-b border-b-white mx-4">
          <Image src={Logo} alt="logo" className="w-32 " />
        </div>
        <Menu
          theme="dark"
          mode="inline"
          className="mt-4"
          style={{ background: "transparent" }}
          defaultSelectedKeys={[selectedMenu]}
        >
          <Menu.Item key="users" icon={<TeamOutlined />} className="text-white">
            <Link href="/users">User Management</Link>
          </Menu.Item>
          <Menu.Item
            key="qas"
            icon={<QuestionCircleOutlined />}
            className="text-white"
          >
            <Link href="/qas">Q&A Management</Link>
          </Menu.Item>
          <Menu.Item
            key="events"
            icon={<CalendarOutlined />}
            className="text-white"
          >
            <Link href="/events">Event Management</Link>
          </Menu.Item>
          <Menu.Item
            key="insights"
            icon={<FormOutlined />}
            className="text-white"
          >
            <Link href="/insights">Insights</Link>
          </Menu.Item>
          <Menu.ItemGroup>
            <Menu.Item
              key="/logout"
              icon={<LogoutOutlined />}
              className="text-white"
              onClick={logout}
            >
              Logout
            </Menu.Item>
          </Menu.ItemGroup>
        </Menu>
      </Sider>
      <Layout style={{ marginLeft: 200 }}>
        <Header
          className="flex items-center h-24 shadow-md"
          style={{ background: colorBgContainer }}
        >
          <div className="flex justify-center w-full grow">
            <Image src={TransparentLogo} alt="logo" className="w-32" />
          </div>
        </Header>
        <Content
          className="m-6 p-6 shadow-md"
          style={{ background: colorBgContainer }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
