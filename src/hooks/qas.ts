import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { message } from "antd";
import { get } from "lodash";

import QA from "@/models/QA";
import { Filter } from "@/models/Filter";
import {
  createQA,
  deleteQA,
  getAllQAs,
  getQADetail,
  updateQA,
} from "@/apis/qas";
import { useShowError } from "@/utils/hooks";
import { DEFAULT_FILTER } from "@/utils/constants";

export const useGetAllQAs = (filter: Filter = DEFAULT_FILTER) => {
  const query = useQuery<{ qas: QA[]; total: number }>(
    ["get_all_qas", filter],
    () => getAllQAs(filter)
  );
  return {
    qas: query.data?.qas,
    total: query.data?.total,
    isLoadingQAs: query.isLoading,
  };
};

export const useGetQADetail = ({
  qaId,
  enabled = true,
}: {
  qaId: string;
  enabled?: boolean;
}): { qa: QA; isLoadingQADetail: boolean } => {
  const query = useQuery(["get_qa_detail", qaId], () => getQADetail(qaId), {
    enabled,
  });
  return { qa: query.data, isLoadingQADetail: query.isLoading };
};

export const useCreateQA = ({ onSuccess }: { onSuccess?: () => void }) => {
  const { showError } = useShowError();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation(createQA, {
    onSuccess: () => {
      message.success("Create Q&A Successfully!");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Create Q&A Failed!", errorMessages);
    },
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ["get_all_qas"] });
    },
  });

  return { createQA: mutate, isCreatingQA: isLoading };
};

export const useUpdateQA = ({ onSuccess }: { onSuccess?: () => void }) => {
  const { showError } = useShowError();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation(updateQA, {
    onSuccess: () => {
      message.success("Update Q&A Successfully!");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Update Q&A Failed!", errorMessages);
    },
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ["get_all_qas"] });
    },
  });

  return { updateQA: mutate, isUpdatingQA: isLoading };
};

export const useDeleteQA = ({ onSuccess }: { onSuccess?: () => void }) => {
  const { showError } = useShowError();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation(deleteQA, {
    onSuccess: () => {
      message.success("Delete Q&A Successfully!");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Delete Q&A Failed!", errorMessages);
    },
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ["get_all_qas"] });
    },
  });

  return { deleteQA: mutate, isDeletingQA: isLoading };
};
