import { useMutation } from "@tanstack/react-query";
import { message } from "antd";
import { get } from "lodash";
import { useRouter } from "next/router";

import { useShowError } from "@/utils/hooks";
import { setAuth, setCurrentUser } from "@/utils/functions";
import { register, login } from "@/apis/auth";

export const useLogin = ({ onSuccess }: { onSuccess?: () => void }) => {
  const router = useRouter();
  const { showError } = useShowError();

  const { mutate, isLoading } = useMutation(login, {
    onSuccess: (data) => {
      message.success("Login successfully!");
      const { accessToken, refreshToken, userId } = data;
      setAuth({ accessToken, refreshToken });
      setCurrentUser({
        id: userId,
        permissions: [],
      });
      router.push("/users");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Login failed!", errorMessages);
    },
  });

  return {
    login: mutate,
    isLoggingin: isLoading,
  };
};

export const useRegister = ({ onSuccess }: { onSuccess?: () => void }) => {
  const router = useRouter();
  const { showError } = useShowError();

  const { mutate, isLoading } = useMutation(register, {
    onSuccess: () => {
      message.success(
        "Sign Up successfully! Please log in to start using our application."
      );
      router.push("/login");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Register failed!", errorMessages);
    },
  });

  return {
    register: mutate,
    isRegistering: isLoading,
  };
};
