import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { message } from "antd";
import { get } from "lodash";

import Event from "@/models/Event";
import { Filter } from "@/models/Filter";
import {
  createEvent,
  deleteEvent,
  getAllEvents,
  getEventDetail,
  updateEvent,
} from "@/apis/events";
import { useShowError } from "@/utils/hooks";
import { DEFAULT_FILTER } from "@/utils/constants";

export const useGetAllEvents = (filter: Filter = DEFAULT_FILTER) => {
  const query = useQuery<{ events: Event[]; total: number }>(
    ["get_all_events", filter],
    () => getAllEvents(filter)
  );
  return {
    events: query.data?.events,
    total: query.data?.total,
    isLoadingEvents: query.isLoading,
  };
};

export const useGetEventDetail = ({
  eventId,
  enabled = true,
}: {
  eventId: string;
  enabled?: boolean;
}): { event: Event; isLoadingEventDetail: boolean } => {
  const query = useQuery(
    ["get_event_detail", eventId],
    () => getEventDetail(eventId),
    {
      enabled,
    }
  );
  return { event: query.data, isLoadingEventDetail: query.isLoading };
};

export const useCreateEvent = ({ onSuccess }: { onSuccess?: () => void }) => {
  const { showError } = useShowError();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation(createEvent, {
    onSuccess: () => {
      message.success("Create Event Successfully!");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Create Event Failed!", errorMessages);
    },
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ["get_all_events"] });
    },
  });

  return { createEvent: mutate, isCreatingEvent: isLoading };
};

export const useUpdateEvent = ({ onSuccess }: { onSuccess?: () => void }) => {
  const { showError } = useShowError();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation(updateEvent, {
    onSuccess: () => {
      message.success("Update Event Successfully!");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Update Event Failed!", errorMessages);
    },
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ["get_all_events"] });
    },
  });

  return { updateEvent: mutate, isUpdatingEvent: isLoading };
};

export const useDeleteEvent = ({ onSuccess }: { onSuccess?: () => void }) => {
  const { showError } = useShowError();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation(deleteEvent, {
    onSuccess: () => {
      message.success("Delete Event Successfully!");
      onSuccess?.();
    },
    onError: (error) => {
      const errorMessages = get(error, "response.data.errors") || [];
      showError("Delete Event Failed!", errorMessages);
    },
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ["get_all_events"] });
    },
  });

  return { deleteEvent: mutate, isDeletingEvent: isLoading };
};
