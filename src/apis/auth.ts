import Axios from "axios";

import LoginRequest from "@/models/LoginRequest";
import RegisterRequest from "@/models/RegisterRequest";
import { API_BASE_URL } from "@/utils/constants";

export const login = async ({ userName, password }: LoginRequest) => {
  const response = await Axios.post(`${API_BASE_URL}/auth/login`, {
    userName,
    password,
    isFromAdmin: true,
  });
  return response.data;
};

export const register = async ({ userName, password }: RegisterRequest) => {
  const response = await Axios.post(`${API_BASE_URL}/auth/register`, {
    userName,
    password,
  });
  return response.data;
};

export const refreshToken = async (refreshToken: string) => {
  const response = await Axios.post(`${API_BASE_URL}/auth/refresh-token`, {
    refreshToken,
  });
  return response.data;
};
