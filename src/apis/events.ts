import Axios from "axios";

import Event, { NewEvent } from "@/models/Event";
import { Filter } from "@/models/Filter";
import { API_BASE_URL } from "@/utils/constants";
import { requestParamsFromObject } from "@/utils/functions";

export const getAllEvents = async (filter: Filter) => {
  const response = await Axios.get(
    `${API_BASE_URL}/events${requestParamsFromObject({
      ...filter,
    })}`
  );
  return response.data;
};

export const getEventDetail = async (eventId: string) => {
  const response = await Axios.get(`${API_BASE_URL}/events/${eventId}`);
  return response.data;
};

export const createEvent = async (event: NewEvent) => {
  const response = await Axios.post(`${API_BASE_URL}/events`, {
    ...event,
  });
  return response.data;
};

export const updateEvent = async (event: Event) => {
  const response = await Axios.patch(`${API_BASE_URL}/events/${event.id}`, {
    ...event,
  });
  return response.data;
};

export const deleteEvent = async ({ eventId }: { eventId: string }) => {
  const response = await Axios.delete(`${API_BASE_URL}/events/${eventId}`);
  return response.data;
};
