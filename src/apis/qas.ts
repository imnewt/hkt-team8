import Axios from "axios";

import QA, { NewQA } from "@/models/QA";
import { Filter } from "@/models/Filter";
import { API_BASE_URL } from "@/utils/constants";
import { requestParamsFromObject } from "@/utils/functions";

export const getAllQAs = async (filter: Filter) => {
  const response = await Axios.get(
    `${API_BASE_URL}/qas${requestParamsFromObject({
      ...filter,
    })}`
  );
  return response.data;
};

export const getQADetail = async (qaId: string) => {
  const response = await Axios.get(`${API_BASE_URL}/qas/${qaId}`);
  return response.data;
};

export const createQA = async (qa: NewQA) => {
  const response = await Axios.post(`${API_BASE_URL}/qas`, {
    ...qa,
  });
  return response.data;
};

export const updateQA = async (qa: QA) => {
  const response = await Axios.patch(`${API_BASE_URL}/qas/${qa.id}`, {
    ...qa,
  });
  return response.data;
};

export const deleteQA = async ({ qaId }: { qaId: string }) => {
  const response = await Axios.delete(`${API_BASE_URL}/qas/${qaId}`);
  return response.data;
};
